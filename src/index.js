const axios = require("axios"); // INFO ABOUT API: https://github.com/axios/axios
const low = require("lowdb"); // INFO ABOUT API: https://github.com/typicode/lowdb
const lowFS = require("lowdb/adapters/FileAsync"); // USE THIS BACKEND AND THE FOLLOWING ADAPTER.

const adapter = new lowFS("db.json");
const db = low(adapter);

/**
 * Cache all users from API request. They should be stored
 * under `users.all`.
 */
async function cacheAllUsers(data) {}

/**
 * Retrieve all users from lowDB. Should be the exact same
 * output as the JSON from the API.
 *
 * The API is listed below and it is a "GET" request.
 * https://reqres.in/api/users?page=1
 */
async function retrieveAllUsers() {}

/**
 * Query, cache, and return all users from API.
 * This should contain a call using `axios` and
 * a call to the local database to *store*.
 */
async function getAllUsers() {}

/**
 * This function should get the user by the last name
 * provided and respond with the following object if not found.
 *
 * {
 *   err: "USER_NOT_FOUND",
 *   msg: "Failed to find user by last name."
 * }
 */
async function getUserByLastName(lastName) {}

/*
EXAMPLE JSON RESPONSE FROM API:

{
  "page": 2,
  "per_page": 6,
  "total": 12,
  "total_pages": 2,
  "data": [
      {
          "id": 7,
          "email": "michael.lawson@reqres.in",
          "first_name": "Michael",
          "last_name": "Lawson",
          "avatar": "https://reqres.in/img/faces/7-image.jpg"
      },
      {
          "id": 8,
          "email": "lindsay.ferguson@reqres.in",
          "first_name": "Lindsay",
          "last_name": "Ferguson",
          "avatar": "https://reqres.in/img/faces/8-image.jpg"
      },
      {
          "id": 9,
          "email": "tobias.funke@reqres.in",
          "first_name": "Tobias",
          "last_name": "Funke",
          "avatar": "https://reqres.in/img/faces/9-image.jpg"
      },
      {
          "id": 10,
          "email": "byron.fields@reqres.in",
          "first_name": "Byron",
          "last_name": "Fields",
          "avatar": "https://reqres.in/img/faces/10-image.jpg"
      },
      {
          "id": 11,
          "email": "george.edwards@reqres.in",
          "first_name": "George",
          "last_name": "Edwards",
          "avatar": "https://reqres.in/img/faces/11-image.jpg"
      },
      {
          "id": 12,
          "email": "rachel.howell@reqres.in",
          "first_name": "Rachel",
          "last_name": "Howell",
          "avatar": "https://reqres.in/img/faces/12-image.jpg"
      }
  ],
  "support": {
      "url": "https://reqres.in/#support-heading",
      "text": "To keep ReqRes free, contributions towards server costs are appreciated!"
  }
}*/

module.exports = {
  getAllUsers: getAllUsers,
  retrieveAllUsers: retrieveAllUsers,
  getUserByLastName: getUserByLastName,
};
