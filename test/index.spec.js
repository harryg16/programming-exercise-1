const assert = require("assert");
const fs = require("fs/promises");
const lib = require("../src/index.js");

describe("Interview", function () {
  describe("#getAllUsers", function () {
    it("should return the correct list of users.", async () => {
      assert.strictEqual(
        JSON.stringify(await lib.getAllUsers()),
        JSON.stringify({
          page: 1,
          per_page: 6,
          total: 12,
          total_pages: 2,
          data: [
            {
              id: 1,
              email: "george.bluth@reqres.in",
              first_name: "George",
              last_name: "Bluth",
              avatar: "https://reqres.in/img/faces/1-image.jpg",
            },
            {
              id: 2,
              email: "janet.weaver@reqres.in",
              first_name: "Janet",
              last_name: "Weaver",
              avatar: "https://reqres.in/img/faces/2-image.jpg",
            },
            {
              id: 3,
              email: "emma.wong@reqres.in",
              first_name: "Emma",
              last_name: "Wong",
              avatar: "https://reqres.in/img/faces/3-image.jpg",
            },
            {
              id: 4,
              email: "eve.holt@reqres.in",
              first_name: "Eve",
              last_name: "Holt",
              avatar: "https://reqres.in/img/faces/4-image.jpg",
            },
            {
              id: 5,
              email: "charles.morris@reqres.in",
              first_name: "Charles",
              last_name: "Morris",
              avatar: "https://reqres.in/img/faces/5-image.jpg",
            },
            {
              id: 6,
              email: "tracey.ramos@reqres.in",
              first_name: "Tracey",
              last_name: "Ramos",
              avatar: "https://reqres.in/img/faces/6-image.jpg",
            },
          ],
          support: {
            url: "https://reqres.in/#support-heading",
            text:
              "To keep ReqRes free, contributions towards server costs are appreciated!",
          },
        })
      );
    });
    it("should create a database file with the correct contents.", async () => {
      assert.strictEqual(
        JSON.stringify(JSON.parse((await fs.readFile("./db.json")).toString())),
        JSON.stringify({
          users: {
            all:
              '{"page":1,"per_page":6,"total":12,"total_pages":2,"data":[{"id":1,"email":"george.bluth@reqres.in","first_name":"George","last_name":"Bluth","avatar":"https://reqres.in/img/faces/1-image.jpg"},{"id":2,"email":"janet.weaver@reqres.in","first_name":"Janet","last_name":"Weaver","avatar":"https://reqres.in/img/faces/2-image.jpg"},{"id":3,"email":"emma.wong@reqres.in","first_name":"Emma","last_name":"Wong","avatar":"https://reqres.in/img/faces/3-image.jpg"},{"id":4,"email":"eve.holt@reqres.in","first_name":"Eve","last_name":"Holt","avatar":"https://reqres.in/img/faces/4-image.jpg"},{"id":5,"email":"charles.morris@reqres.in","first_name":"Charles","last_name":"Morris","avatar":"https://reqres.in/img/faces/5-image.jpg"},{"id":6,"email":"tracey.ramos@reqres.in","first_name":"Tracey","last_name":"Ramos","avatar":"https://reqres.in/img/faces/6-image.jpg"}],"support":{"url":"https://reqres.in/#support-heading","text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}',
          },
        })
      );
    });
    it("should fail to find a user with last name 'Gould'", async () => {
      assert.strictEqual(
        JSON.stringify(await lib.getUserByLastName("Gould")),
        JSON.stringify({
          err: "USER_NOT_FOUND",
          msg: "Failed to find user by last name.",
        })
      );
    });
    it("should find a user with last name 'Wong'", async () => {
      assert.strictEqual(
        JSON.stringify(await lib.getUserByLastName("Wong")),
        JSON.stringify({
          id: 3,
          email: "emma.wong@reqres.in",
          first_name: "Emma",
          last_name: "Wong",
          avatar: "https://reqres.in/img/faces/3-image.jpg",
        })
      );
    });
  });
});
