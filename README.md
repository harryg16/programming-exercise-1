# Programming Exercise 1

Requirements:

- `nodejs`
- `npm`

For installation instructions see: [the official Node.js website](https://nodejs.org/en/).

The repository is very minimal, but you will find the file you need to edit is in the `src` directory and called `index.js`. First, run a `npm install` in the root directory and `npm i -g mocha` in order to install mocha. The initial output when running `mocha` in the root directory should be:

```
$ mocha

  Interview
    #getAllUsers
      1) should return the correct list of users.
      2) should create a database file with the correct contents.
      3) should fail to find a user with last name 'Gould'
      4) should find a user with last name 'Wong'


  0 passing (9ms)
  4 failing
```

Write the necessary code to the four functions in the skeleton file which are listed below.

```js
/**
 * Cache all users from API request. They should be stored
 * under `users.all`.
 */
async function cacheAllUsers(data) {}

/**
 * Retrieve all users from lowDB. Should be the exact same
 * output as the JSON from the API.
 *
 * The API is listed below and it is a "GET" request.
 * https://reqres.in/api/users?page=1
 */
async function retrieveAllUsers() {}

/**
 * Query, cache, and return all users from API.
 * This should contain a call using `axios` and
 * a call to the local database to *store*.
 */
async function getAllUsers() {}

/**
 * This function should get the user by the last name
 * provided and respond with the following object if not found.
 *
 * {
 *   err: "USER_NOT_FOUND",
 *   msg: "Failed to find user by last name."
 * }
 */
async function getUserByLastName(lastName) {}
```

The output of running the program upon completion should be as follows:

```
$ mocha


  Interview
    #getAllUsers
      ✓ should return the correct list of users. (52ms)
      ✓ should create a database file with the correct contents.
      ✓ should fail to find a user with last name 'Gould'
      ✓ should find a user with last name 'Wong'


  4 passing (58ms)

```

Documentation on all resources can be found below:

- `axios` package: https://github.com/axios/axios
- `lowdb` package: https://github.com/typicode/lowdb
- `mocha` package: https://mochajs.org/